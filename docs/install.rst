.. test documentation master file, created by
   sphinx-quickstart on Wed Mar 30 15:11:33 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Installation Document!
================================
Installation

At the command line:

easy_install crawler

Or, if you have pip installed:

pip install crawler


